const PRIORITIES: &str = "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

fn main() {
    let input = include_str!("../input.txt");
    println!("Part One Solution: {}", solve_part_one(input));
    println!("Part Two Solution: {}", solve_part_two(input));
}

#[test]
fn test_solve_part_one() {
    let test_input = include_str!("../test.txt");
    assert_eq!(solve_part_one(test_input), 157);
}

fn solve_part_one(input: &str) -> u32 {
    let mut priorities_sum: u32 = 0;

    input.lines().for_each(|line| {
        let (compartment_a, compartment_b) = line.split_at(line.len() / 2);
        for item in compartment_a.chars() {
            if compartment_b.contains(item) {
                priorities_sum += PRIORITIES.find(item).unwrap_or(0) as u32;
                break;
            }
        }
    });

    priorities_sum
}

#[test]
fn test_solve_part_two() {
    let test_input = include_str!("../test.txt");
    assert_eq!(solve_part_two(test_input), 70);
}

fn solve_part_two(input: &str) -> u32 {
    let mut priorities_sum: u32 = 0;
    let lines_as_vec = input.lines().collect::<Vec<&str>>();
    let lines_by_three = lines_as_vec.chunks(3);

    for group in lines_by_three {
        for item in group[0].chars() {
            let members_containing = group.iter().filter(|member| member.contains(item)).count();
            if members_containing == 3 {
                priorities_sum += PRIORITIES.find(item).unwrap_or(0) as u32;
                break;
            }
        }
    }

    priorities_sum
}
